########################################################
#
#   
#   _____          __  __       _____ _ _            _     
#   |_   _|   /\   |  \/  |     / ____| (_)          | |   
#     | |    /  \  | \  / |    | |    | |_  ___ _ __ | |_  
#     | |   / /\ \ | |\/| |    | |    | | |/ _ \ '_ \| __| 
#    _| |_ / ____ \| |  | |    | |____| | |  __/ | | | |_  
#   |_____/_/    \_\_|  |_|     \_____|_|_|\___|_| |_|\__| 
#                                                                                               
#
#   IAM Client Excercise
#
#   This excercise should give you a good start on what
#   is possible with the IAMClient PowerShell Module!
#   
#   This excercise was originally written for the 
#   PowerShell course in December 2019.
#   
#   For any Questions, contact me (aurels [ÄT] ethz.ch)
#
#   ###################################################
# 
#   How to use
#
#   1) Open the file in Visual Studio Code (vscode) https://code.visualstudio.com/
#   2) Install PowerShell extension https://code.visualstudio.com/docs/languages/powershell
#   3) Install IAMClient
#      PS> Install-Module iamclient
#
#   4) Edit the relevant lines where # ... completehere  is mentioned
#      -> Select the code that you want to run and press F8
#      -> vscode will then automatically run it in the integrated console
#
#   5) Tip: All commands are documented, use this to get example usages:
#      PS> Get-Help Get-ETHGroup -Examples  
#      -> Try it -  select the command, without 'PS>' and press F8!
#   5) Now have fun!
#


# this prevents you from running this script with F5
return

#######################################################
#
# Chapter 1: Introduction
#
#

# Task 1.0: Familiarize yourself with the available commands:
Get-Command -Module IAMClient

######

# Task 1.1: Initialize the Client -> Login
Initialize-IAMClient -SaveCredential

######

# Task 1.2: Count the nr of personas of 'aurels' that start with "asc4"
#           Some starting code is provided
#
#           Warning: If you have not spent some time with PowerShell
#           before this excercise, skip it!
#
$aurels = Get-ETHPerson aurels
$Usernames = $aurels.usernames | Where-Object # ... complete here

# Verify results (should be 7)
$Usernames | Measure-Object

######

# Task 1.3: Create a group
#            -> In your admingroup (D-BIOL, ID-S4D, etc)
#            -> With a name that contains "test" somewhere (for others to see why this exists)
#            -> !! Exported to AD ONLY !!
#
#            Help: Get-Help New-ETHGroup -Examples
New-ETHGroup # ...


######


# Task 1.4: Add yourself and *your current user* to the group
#           Help: Get-Help Add-ETHGroupMember -Examples
Add-ETHGroupMember -Identity <# ... #> -Members <# ... #>

# Verify results
Get-ADGroupMember -Identity # ... completehere
Get-ETHGroupMember -Identity # ... completehere

######

## Task 1.5: Remove your group again
#            Help: Get-Help Remove-ETHGroup -Examples
Remove-ETHGroup


#######################################################
#
# Chapter 2: Working with users
#
#
## Task 2.1: View all groups that you are a member
#            Help: Get-Help Get-ETHUserGroupMembership -Examples
Get-ETHUserGroupMembership # ... completehere


######


## Task 2.2: Create a new "Persona" for your user
New-ETHPersona -ParentIdentity <# complete here #> -NewUserName <# complete here #> -InitPwd <# warning, this is stored in plaintext in IAM! #>

# Info: See task 2.4 for the "correct" way of setting the password!

######

## Task 2.3: Assign services (Mailbox, Radius) to your new created persona
Add-ETHUserITService -ITServiceName "Mailbox" -Identity # ... completehere
Add-ETHUserITService -ITServiceName "LDAP" -Identity # ... completehere

# Info: Use   -ITServiceName "WLAN_VPN"   for Radius!

######

## Task 2.4: Set the password for the newly given services
$Password = Read-Host -AsSecureString -Prompt "Enter the new password"
Reset-ETHUserPassword -NewPassword $Password # ... completehere


######

## Task 2.5: Add yourself a new mailalias "<username>_hello_world@<domain>.ethz.ch"
Add-ETHUserMailAlias -Identity

######

## Task 2.6: Remove the alias again
# Sadly, the IAM Api does not allow to remove aliases, use https://iam.password.ethz.ch for this :(

######

## Task 2.7: Change the homedrive of your user
#            First, view your properties (and remember what it stored!)
Get-ETHUser # ... completehere

#            Then: set it to "S:" with HomeDir \\server.ethz.ch\home\%username%
Set-ETHUser # ... completehere

## Task 2.8: Reset your changes
#            Change your homedrive and HomeDir back to whatever it was before :).
#            *scroll up in powershell history if you forgot*

# ... completehere (no starting code provided :) )












######################## SOLUTIONS
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########


#
## Chapter 1
#

# Task 1.2
$aurels = Get-ETHPerson "aurels"
$usernames = $aurels.usernames | Where-Object username -like "asc4*" | Measure-Object


# Task 1.3
# Example provided:
New-ETHGroup -Name "biol-micro-testgroup-tobedel" -Description "testgroup" -Targets AD -AdminGroup "D-BIOL"


# Task 1.4
Add-ETHGroupMember -Identity "biol-micro-testgroup-tobedel" -Members $env:USERNAME,"aurels"

Get-ETHGroupMember -Identity "biol-micro-testgroup-tobedel"
Get-ADGroupMember -Identity "biol-micro-testgroup-tobedel"


# Task 1.5
Remove-ETHGroup -Name "biol-micro-testgroup-tobedel"


Get-ETHUserGroupMembership # ... completehere





#
## Chapter 2
#

## Task 2.2: Create a new "Persona" for your user
New-ETHPersona -ParentIdentity "aurels" -NewUserName "aurels4test" -InitPwd "something_random_blabla1234"

## Task 2.3: Assign services (Mailbox, Radius) to your new created persona
Add-ETHUserITService -ITServiceName "Mailbox" -Identity aurels4test
Add-ETHUserITService -ITServiceName "LDAP" -Identity aurels4test

# Optional
Add-ETHUserITService -ITServiceName "WLAN_VN" -Identity aurels4test

## Task 2.4: Set the password for the newly given services
$Password = Read-Host -AsSecureString -Prompt "Enter the new password"
Reset-ETHUserPassword -NewPassword $Password -Identity aurels4test -ServiceNames "Mailbox","LDAP"

# Optional
$Password = Read-Host -AsSecureString -Prompt "Enter the new password for Radius"
Reset-ETHUserPassword -NewPassword $Password -Identity aurels4test -ServiceNames "WLAN VPN"

## Task 2.5: Add yourself a new mailalias "<username>_hello_world@<domain>.ethz.ch"
Add-ETHUserMailAlias -Identity aurels4test -Alias "aurels4test_hello_world@ethz.ch"

## Task 2.7: Change the homedrive of your user
Get-ETHUser -Identity aurels4test
#
Set-ETHUser -Identity aurels4test -HomeDrive "S:" -HomeDirectory "\\server.ethz.ch\home\%username%"

## Task 2.8: Reset your changes
Set-ETHUser -Identity aurels4test -HomeDrive "" -HomeDirectory ""