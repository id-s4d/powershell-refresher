# Install the IAMClient
Install-Module IAMClient

# And install the speculationcontrol module
Install-Module speculationcontrol

# After it's done, we can start using the code
Get-SpeculationControlSettings

# If a module needs an update, use:
Update-Module